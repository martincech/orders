﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OrdersWebApp.Startup))]
namespace OrdersWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
