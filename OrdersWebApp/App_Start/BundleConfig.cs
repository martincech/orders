﻿using System.Web;
using System.Web.Optimization;

namespace OrdersWebApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                      "~/Scripts/respond.js"
                      ));
            
            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                      "~/Scripts/angular.js",
                      "~/Scripts/angular-foundation.js",
                      "~/Scripts/i18n/angular-locale_cs-cz.js",
                      "~/Scripts/angular-sanitize.js",
                      "~/Scripts/angular-animate.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/bower_components").Include(
                      "~/bower_components/moment/moment.js",
                      "~/bower_components/moment-timezone/moment-timezone.js",
                      "~/bower_components/angularjs-datepicker/src/js/angular-datepicker.js"
                      ));

            //bundles.Add(new StyleBundle("~/bundles/css").Include(
            //          "~/Content/css/font-awesome/font-awesome.css",
            //          "~/Content/css/site.css"));
        }
    }
}
