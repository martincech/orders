﻿/*-- ANGULAR --*/

var app = angular.module('OrdersApp', [
        'ngSanitize',
        'mm.foundation',
        //'mm.foundation.accordion',
        //'dndLists',
        '720kb.datepicker',
        //'imageCropper',
        //'angular-carousel',
        'ngAnimate',
        //'vAccordion',
        //'angular-lodash',
        //'vTabs',
        //'api.services',
        //'d3Module',
        //'veitModule',
        //'ngCookies',
        //'datatables'
    ])
    .directive('rightMenu', function() {
        return {
            restrict: 'A',
            link: function(scope, element) {
                element.bind("click", function() {
                    var canvas = document.getElementsByClassName("js-off-canvas-exit")[0];
                    if (!canvas) return;
                    setTimeout(function() { canvas.classList.remove("is-visible") }, 0);
                });
            }
        };
    })
    .controller('ordersCtrl', [
        "$scope", "$http",
        function($scope, $http) {

            $scope.itemsInOrder = [
                {
                    name: 'External module for TMC unit',
                    desc: 'will be mounted on truck',
                    weight: 67,
                    count: 3,
                    price: 1598
                },
                {
                    name: 'Semitrailer for 120.000 chicks',
                    desc: 'deliver as soon as we can',
                    weight: 18.78,
                    count: 1,
                    price: 110598
                },
                {
                    name: 'Něco hodně zvláštního, co má diakritiuku a dlouhý název VELKÝMÍ PÍSMÉNÁ',
                    desc: 'Něco hodně zvláštního, co má diakritiuku a dlouhý název VELKÝMÍ PÍSMÉNÁ' +
                        'max-height: 100% !important; Něco hodně zvláštního, co má diakritiuku a dlouhý název VELKÝMÍ PÍSMÉNÁ Něco hodně zvláštního, co má diakritiuku a dlouhý název VELKÝMÍ PÍSMÉNÁ',
                    weight: 99899,
                    count: 9999,
                    price: 98952663
                },
                {
                    name: 'External module for TMC unit',
                    desc: '',
                    weight: 67,
                    count: 3,
                    price: 1598
                },
                {
                    name: 'External module for TMC unit',
                    desc: 'above has no desc',
                    weight: 67,
                    count: 3,
                    price: 1598
                }
            ];

            

            $scope.company = {
                name: 'Super Worlwide Unnatural Company s.r.o.',
                address: 'Maryland, Arizona, USA',
                postal: 60200,
                vac: 'CZ25963548'
                };

            $scope.orderId = 12345678;
            $scope.orderTotal = 89563325;

            $scope.orderDate = new Date();
            $scope.orderDateFormat = 'dd. MM. yyyy';

            $scope.orderPriceUnit = 'CZK';
            $scope.orderCountUnit = 'pcs';
            $scope.orderWeightUnit = 'kg';

            $scope.order = {
                state: 'NoChanges'
            };

            $scope.activeState = 'NoChanges';

            $scope.dateChanged = function (date) {
                //var dToday = new Date(new Date().setUTCHours(0, 0, 0, 0)),
                ////var orderDate = new Date();
                //orderDate.From = dToday;
                //var from = new Date();
                //if (orderDate.To > from) from = orderDate.To;



            }

//*---end of ordersCtrl---*/
    }]);